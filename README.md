# GestionEnsupSpringSonar

Dépôt de projets de gestion d'école en utilisant les webservices.

## Infos versions

- Webservice Restful : projet maven web multimodule avec Jersey 2.32
- Client : projet maven web multimodule avec Jersey 2.32

## Accès utilisateur

URL : http://18.220.60.106:8080/webclient/  
Login : admin  
Mot de passe : admin  

## Démo

[Vidéo de démonstration](https://www.youtube.com/watch?v=ZCR3_G3agps&ab_channel=BenjaminBoutrois)

## Equipe
   
Yoeko Klu  
Morgan Franca  
Cédric Nozerand  
Benjamin Boutrois
Braham Moussouni
